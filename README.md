<p align="center">
	<a href="https://linuxitos.com" target="_blank">
		<img src="./public/assets/imgs/favicon.png" width="100" alt="Laravel Logo">
	</a>
</p>

<p align="center">
	<a href="https://opensource.org/licenses/MIT">
		<img src="https://img.shields.io/packagist/l/laravel/framework" alt="License">
	</a>
</p>

# Web App con Laravel 10

Un proyecto para comenzar a aprender PHP usando Framework Laravel 10:

## Funcionalidades
- Uso de ajax
- Login
- Registro
- Uso de bootstrap 5.3.2
- Exportar a PDF (MPDF)
- Exportar a Excel
- Paginación personalizada
- Subida de archivo
- Búsqueda ajax
- Modo oscuro y claro	

## Requerimientos

- OS utilizado Fedora 39 x86_64
- PHP 8.2
- Laravel 10
- Bootstrap 5.3.2
- MPDF
- MariaDB
- XAMPP 8.2.12

## Capturas

<img src="./public/assets/imgs/1.png" alt="1">

<img src="./public/assets/imgs/2.png" alt="1">

<img src="./public/assets/imgs/3.png" alt="1">

<img src="./public/assets/imgs/6.png" alt="1">

<img src="./public/assets/imgs/4.png" alt="1">

<img src="./public/assets/imgs/5.png" alt="1">

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
