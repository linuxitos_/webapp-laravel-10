<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Auth::routes();

Route::namespace('App\Http\Controllers')->group(function () {
    Route::match(['get', 'post'], '/', 'MainController@index')->name('/');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/posts', 'HomeController@posts')->name('posts');

    Route::get('account', 'AccountController@account')->name('account');
    Route::post('loadImageUser', 'AccountController@loadImageUser')->name('loadImageUser');
    Route::post('upImgUser', 'AccountController@upImgUser')->name('upImgUser');
    Route::post('upProfile', 'AccountController@upProfile')->name('upProfile');

    Route::match(['get', 'post'], 'add', 'HomeController@add')->name('add');
    Route::match(['get', 'post'], 'load', 'HomeController@load')->name('load');
    Route::match(['get', 'post'], 'edit/{id}', 'HomeController@edit')->name('edit');
    Route::get('/{slug_post}', 'MainController@view');
    Route::match(['get', 'post'], 'upload', 'HomeController@uploadImageCkeditor')->name('uploadImageCkeditor');
    Route::match(['get', 'post'], 'imageckeditor/{segment}', 'HomeController@imageckeditor')->name('imageckeditor');
    Route::post('addPost', 'HomeController@addPost')->name('addPost');
    Route::post('upPost', 'HomeController@upPost')->name('upPost');
    Route::post('loadImagePost', 'HomeController@loadImagePost')->name('loadImagePost');
    Route::post('upImgPost', 'HomeController@upImgPost')->name('upImgPost');

    Route::post('expUsrFile', 'HomeController@expUsrFile')->name('expUsrFile');
});


Route::namespace('App\Http\Controllers')->middleware(['auth'])->group(function () {
    Route::get('editPost/{post}', 'HomeController@editPost')->name('editPost');
    Route::match(['get', 'post'], 'delete', 'HomeController@delete')->name('delete');
});
