<?php
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

if (!function_exists('nicetime')) {
	function nicetime($date){
		if(empty($date)) {
			return "No date provided";
		}

		$periods 	= array("segundo", "minuto", "hora", "día", "semana", "mes", "año", "decada");
		$lengths 	= array("60","60","24","7","4.35","12","10");
		$now 		= time();
		$unix_date 	= strtotime($date);

		   // check validity of date
		if(empty($unix_date)) {
			return "Error en la fecha";
		}

		// is it future date or past date
		if($now > $unix_date) {
			$difference     = $now - $unix_date;
			$tense         = "hace";

		} else {
			$difference     = $unix_date - $now;
			$tense         = "justo ahora";
		}

		for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			$difference /= $lengths[$j];
		}

		$difference = round($difference);

		if($difference != 1) {
			if ($periods[$j]=="mes") {
				$periods[$j].= "es";
			}else{
				$periods[$j].= "s";
			}
		}
		return "{$tense} $difference $periods[$j]";
	}
}

function setSession($attribute = '', $value = '')
{
    session([config('app.name').'_'.$attribute => $value]);
}

function getSession($attribute = '', $value = '')
{
    return session(config('app.name').'_'.$attribute);
}

if (!function_exists('datecustom')) {
	function datecustom($fecha){
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		return "".$meses[date("n", strtotime($fecha))-1].' '.date("d", strtotime($fecha)).", ".date(date("Y", strtotime($fecha)));
	}
}

function fecha($fecha = null, string $formato = 'Y-m-d')
{
    //return optional($fecha)->format($formato);
    $date = Carbon::parse($fecha);
    $formattedDate = $date->format($formato);
    return $formattedDate;
}

function paginate($reload, $page, $tpages, $adjacents, $fuc_load) {
    $prevlabel 	= '<i class="fas fa-chevron-left"></i>';
    $nextlabel 	= '<i class="fas fa-chevron-right"></i>';
    $out 		= '<nav aria-label="..."><ul class="pagination pagination-large justify-content-end">';

    // previous label
    if($page==1) {
        $out .= '<li class="page-item disabled"><a class="page-link">'.$prevlabel.'</a></li>';
    } else if($page==2) {
        $out .= '<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="'.$fuc_load.'(1);">'.$prevlabel.'</a></li>';
    }else {
        $out .= '<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="'.$fuc_load.'('.($page-1).');">'.$prevlabel.'</a></li>';
    }

    // first label
    if($page>($adjacents+1)) {
        $out .= '<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="'.$fuc_load.'(1);">1</a></li>';
    }

    // interval
    if($page>($adjacents+2)) {
        $out .= '<li class="page-item"><a class="page-link">...</a></li>';
    }

    // pages
    $pmin = ($page>$adjacents) ? ($page-$adjacents) : 1;
    $pmax = ($page<($tpages-$adjacents)) ? ($page+$adjacents) : $tpages;
    for($i=$pmin; $i<=$pmax; $i++) {
        if($i==$page) {
            $out .= '<li class="page-item active"><a class="page-link">'.$i.'</a></li>';
        }else if($i==1) {
            $out .= '<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="'.$fuc_load.'(1);">'.$i.'</a></li>';
        }else {
            $out .= '<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="'.$fuc_load.'('.$i.');">'.$i.'</a></li>';
        }
    }

    // interval
    if($page<($tpages-$adjacents-1)) {
        $out .= '<li class="page-item"><a class="page-link">...</a></li>';
    }

    // last
    if($page<($tpages-$adjacents)) {
        $out .= '<li class="page-item"><a class="page-link" href="javascript:void(0);" onclick="'.$fuc_load.'('.$tpages.');">'.$tpages.'</a></li>';
    }

    // next
    if($page<$tpages) {
        $out .= '<li class="page-item"><a class="page-link" href="javascript:void(0);"" onclick="'.$fuc_load.'('.($page+1).');">'.$nextlabel.'</a></li>';
    }else {
        $out .= '<li class="page-item disabled"><a class="page-link">'.$nextlabel.'</a></li>';
    }

    $out .= '</ul></nav>';
    return $out;
}
