<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Propaganistas\LaravelFakeId\RoutesWithFakeIds;

class Post extends Model
{
    use RoutesWithFakeIds;

    protected $table = 'posts';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = ['nom_post', 'cnt_post', 'slug_post', 'img_post', 'act_post', 'fc_post', 'user_id'];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
