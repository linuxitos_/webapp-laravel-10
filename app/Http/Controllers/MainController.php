<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data['tab'] = 'index';
        $data = Post::where('act_post', '=', 2)
					->orderBy('fc_post', 'desc')
					->paginate(6);
		return view('main', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function view($slug)
    {
        $data['tab'] = 'view';
        $data['post'] = Post::where('slug_post', $slug)->first();

        $data['latest'] = Post::where('act_post', '=', 2)
        ->orderBy('fc_post', 'desc')->limit(3)->get();

        return view('view', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
