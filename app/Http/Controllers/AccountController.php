<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    protected $model;
    protected $validationRules;
    protected $validationUpRules;
    protected $attributeNames;
    protected $errorMessages;

    public function __construct()
    {
        $this->middleware('auth');

        $this->validationRules = [
            'name' => 'required|string|max:250',
        ];
        $this->attributeNames = [
            'name' => 'nombre',
        ];
        $this->errorMessages = [
            'required' => 'El campo :attribute es obligatorio.',
        ];

    }

    public function account()
    {
        return view('account');
    }

    public function loadImageUser(Request $request)
    {
        $res['results'] = '<figure class="figure"><img src="'.(!is_null(auth()->user()->avatar) && auth()->user()->avatar!=''? asset(auth()->user()->avatar) : asset('assets/imgs/user.svg')).'" class="figure-img img-fluid rounded" alt="Image"></figure>';
        return response()->json($res);
    }

    public function upImgUser(Request $request)
    {
        if ($request->file) {
            $path = 'uploads/images/';
            $image = Str::random(4).'-'.Str::random(4);
            $imageName = $image.'.'.$request->file->getClientOriginalExtension();

            $request->file->move($path, $imageName);

            $datos['avatar'] = $path.$imageName;
            User::where('id', auth()->user()->id)->update($datos);
            $msg = [
                'msg' => 'Imagen actualizada correctamente',
                'tipo' => 'success',
                'icon' => 'bi bi-check-circle',
            ];
        } else {
            $msg = ['type' => 'danger',
                'icon' => 'bi bi-x-circle',
                'msg' => 'Seleccione al menos una imagen.', ];
        }

        return response()->json($msg);
    }

    public function upProfile(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validationRules, $this->errorMessages)->setAttributeNames($this->attributeNames);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        } else {
            $datos = $request->except(['id', '_token']);
            if (auth()->user()->update($datos) >= 0) {
                $msg = [
                    'tipo' => 'success',
                    'icon' => 'bi bi-check-circle',
                    'url' => route('account'),
                    'msg' => 'Registro actualizado',
                ];
            } else {
                $msg = [
                    'tipo' => 'danger',
                    'icon' => 'bi bi-x-circle',
                    'msg' => 'Error con el servidor de base de datos.',
                ];
            }
        }

        return response()->json($msg);
    }
}
