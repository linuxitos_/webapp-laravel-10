<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;
use \Mpdf\Mpdf;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $model;
    protected $validationRules;
    protected $validationUpRules;
    protected $attributeNames;
    protected $errorMessages;

    public function __construct()
    {
        $this->middleware('auth');

        $this->validationRules = [
            'nom_post' => 'required|string|max:250',
            'fc_post' => 'required|date',
            'cnt_post' => 'nullable|string|max:500',
            'img_post' => 'nullable|string|max:500',
        ];
        $this->validationUpRules = [
            'nom_post' => 'required|string|max:250',
            'fc_post' => 'required|date',
            'cnt_post' => 'nullable|string|max:500',
        ];
        $this->attributeNames = [
            'nom_post' => 'título',
            'fc_post' => 'fecha',
            'cnt_post' => 'contenido',
            'img_post' => 'imagen',
        ];
        $this->errorMessages = [
            'required' => 'El campo :attribute es obligatorio.',
        ];

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['tab'] = 'home';
        return view('home');
    }

    public function home()
    {
        return view('home');
    }

    public function posts()
    {
        return view('posts');
    }

    public function add()
    {
        return view('add');
    }

    public function load(Request $request){
		$userId 			= auth()->user()->id;
		$input['user_id'] 	= $userId;

		$data['page'] 		= ($request->input('page')) 	? $request->input('page'): 1;
		$data['order'] 		= ($request->input('order')) 	? $request->input('order'): "desc";
		$data['order_by'] 	= ($request->input('order_by')) ? $request->input('order_by'): "id";
		$data['search'] 	= ($request->input('search')) 	? trim($request->input('search')) : "";
		$data['per_page'] 	= ($request->input('limite')) 	? $request->input('limite'): 10;
		$data['filter'] 	= ($request->input('filter')) 	? explode(',', $request->input('filter')): array(1);;
		$data['offset'] 	= ($data['page'] - 1) * $data['per_page'];
		$data['adyacentes'] = 2;

		$total = DB::table('posts')
			->join('users', 'users.id', '=', 'posts.user_id')
			->select('posts.*', 'users.*')
			->where('posts.user_id', '=', $userId)
            ->whereIn('act_post', $data['filter'])
			->where('nom_post', 'like', '%'.$data['search'].'%')
			->count();

		$results = Post::select('posts.*')
			->join('users', 'users.id', '=', 'posts.user_id')
			->select('posts.*')
			->where('posts.user_id', '=', $userId)
			->where('nom_post', 'like', '%'.$data['search'].'%')
            ->whereIn('act_post', $data['filter'])
			->offset($data['offset'])
			->limit($data['per_page'])
			->orderBy($data['order_by'], $data['order'])
			->get();

        setSession('regUsersExp', $results);

		$total_pages = ceil($total/$data['per_page']);
		$reload 			= url('/page');
		$response['total']  = $total;

		$response['data'] 	= "";
		if ($total>0) {
			$response['data'] .= '<table class="table table-bordered table-hover">
				<thead class="thead-light table-primary">
					<tr class="row-link">
						<th data-field="id" class="th-link w-10 text-center" title="Click para ordenar">'.($data['order_by']=='id'?($data['order']=='asc'?'<i class="bi bi-sort-down-alt"></i>':'<i class="bi bi-sort-down"></i>'):'<i class="bi bi-filter"></i>').' #</th>
						<th data-field="nom_post" class="th-link" title="Click para ordenar">'.($data['order_by']=='nom_post'?($data['order']=='asc'?'<i class="bi bi-sort-down-alt"></i>':'<i class="bi bi-sort-down"></i>'):'<i class="bi bi-filter"></i>').' Nombre</th>
						<th data-field="act_post" class="w-15 th-link text-center" title="Click para ordenar">'.($data['order_by']=='act_post'?($data['order']=='asc'?'<i class="bi bi-sort-down-alt"></i>':'<i class="bi bi-sort-down"></i>'):'<i class="bi bi-filter"></i>').' Estado</th>
						<th class="w-15 text-center">Acción</th>
					</tr>
				</thead>
				<tbody>';
			foreach ($results as $post) {
				$response['data'] .='<tr>
							<th class="text-center">
								'.$post->id.'
							</th>
							<td>
								<a href="'.('/'.$post->slug_post).'">'.$post->nom_post.'</a>
							</td>
							<td class="text-center">
								'.($post->act_post==1?'<span class="badge text-bg-success"><i class="bi bi-lock"></i> Privado</span>':'<span class="badge text-bg-danger"><i class="bi bi-globe"></i> Publicado</span>').'
							</td>
							<td class="text-center">
								<a href="'.route('editPost', $post).'" class="btn btn-link">
                                    <i class="bi bi-pencil-square text-"></i>
								</a>
								<button type="button" id="btn-del-'.$post->id.'" name="btn-del-'.$post->id.'" class="btn btn-link mdl-del-reg" data-bs-toggle="modal" data-bs-target="#mdl-del-reg" data-idreg="'.$post->id.'" data-nomreg="'.$post->nom_post.'">
                                    <i class="bi bi-trash3 text-danger"></i>
								</button>
							</td>
						</tr>';
			}
			$response['data'] .= '</tbody></table>';
			$response['data'] .= '<span class="pull-right">'.paginate($reload, $data['page'], $total_pages, $data['adyacentes'], 'load').'</span>';
		}else{
			$response['data'] 	= '<div class="alert alert-info text-center" role="alert"><i class="fas fa-search"></i> Búsqueda sin resultados.</div>';
		}

		return response()->json($response);
	}

    public function editPost(Post $post)
    {
        return view('edit', compact('post'));
    }

    public function delete(Request $request){
		$post = Post::find($request->id);
		if ($post->delete()) {
			$msg = array("type" => 'success',
						"icon" 	=> 'fa fa-check',
						"msg" 	=> 'Registro eliminado correctamente.');
		} else {
			$msg = array("type" => 'danger',
						"icon" 	=> 'bi bi-exclamation-circle',
						"msg" 	=> 'Error con la base de datos.');
		}

		return response()->json($msg);
	}

    public function uploadImageCkeditor(Request $request)
    {
        $response = [
            'uploaded' => false,
            "url" => "",
        ];
        $file = $request->upload;
        $nom_img = 'ck-' . fraseAleatoria(10) . '-' . fraseAleatoria(10) . '.' . $file->getClientOriginalExtension();
        /*$data['dir_files'] = 'files/ckeditorimgs/';
        if (!is_dir(env('pathFile') . $data['dir_files'])) {
            mkdir(env('pathFile') . $data['dir_files'], 0775, TRUE);
        }*/
        if ($file->move(env('pathFile') . 'uploads/ckeditor/', $nom_img)) {
            //if(!Storage::disk('public_uploads')->put('uploads/ckeditor', $file)){
            $response = [
                'uploaded' => true,
                "url" => route('imageckeditor', $nom_img),
            ];
        }
        return response()->json($response);
    }

    public function imageckeditor($image = "")
    {
        $data['dir_files'] = 'uploads/ckeditor/';
        if ($image != "" && file_exists(env('pathFile') . $data['dir_files'] . $image)) {
            $img = env('baseFiles') . $data['dir_files'] . $image;
            $mimeType = env('pathFile') . $data['dir_files'] . $image;
        } else {
            $img = env('baseFiles') . 'files/defaults/image404.png';
            $mimeType = 'image/png';
        }
        $contents = file_get_contents($img);
        $response = Response::make($contents, 200);
        $response->header('Content-Type', $mimeType);
        return $response;
    }

    public function addPost(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validationRules, $this->errorMessages)->setAttributeNames($this->attributeNames);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        } else {
            $request['user_id'] = auth()->user()->id;
            if ($reg = Post::create($request->all())) {
                $daUp['img_post'] = 'uploads/images/';
                $daUp['slug_post'] = Str::slug($request->nom_post.'-LM'.$reg->id, '-');
                if ($request->hasfile('files')) {
                    $filesAllowed = ['png', 'jpg', 'jpeg', 'gif', 'svg', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'txt'];
                    $cor = 0;
                    $err = 0;

                    foreach ($request->file('files') as $file) {
                        if (in_array(strtolower($file->getClientOriginalExtension()), $filesAllowed)) {
                            $nomImg = Str::random(4) . '-' . Str::random(4) . '.' . strtolower($file->getClientOriginalExtension());
                            $file->move($daUp['img_post'], $nomImg);
                        }
                    }
                }
                $daUp['img_post'] = $daUp['img_post'].$nomImg;
                $reg->update($daUp);
                $msg = [
                    'tipo' => 'success',
                    'icon' => 'bi bi-check-circle',
                    'url' => route('editPost', $reg),
                    'msg' => 'Registro guardado, redireccionando',
                ];
            } else {
                $msg = [
                    'tipo' => 'danger',
                    'icon' => 'bi bi-x-circle',
                    'msg' => 'Error con el servidor de base de datos.',
                ];
            }
        }

        return response()->json($msg);
    }

    public function upPost(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validationUpRules, $this->errorMessages)->setAttributeNames($this->attributeNames);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        } else {
            $datos = $request->except(['id', '_token']);
            if (Post::where('id', $request->id)->update($datos) >= 0) {
                $reg = Post::find($request->id);
                $msg = [
                    'tipo' => 'success',
                    'icon' => 'bi bi-check-circle',
                    'url' => route('editPost', $reg),
                    'msg' => 'Registro guardado, recargando',
                ];
            } else {
                $msg = [
                    'tipo' => 'danger',
                    'icon' => 'bi bi-x-circle',
                    'msg' => 'Error con el servidor de base de datos.',
                ];
            }
        }

        return response()->json($msg);
    }

    public function loadImagePost(Request $request)
    {
        $reg = $request->reg ? $request->reg : 0;
        $reg = Post::find($reg);
        $res['results'] = '<figure class="figure"><img src="'.(!is_null($reg->img_post) && $reg->img_post!=''? asset($reg->img_post) : asset('assets/imgs/default.png')).'" class="figure-img img-fluid rounded" alt="Image"></figure>';

        return response()->json($res);
    }

    public function upImgPost(Request $request)
    {
        if ($request->file) {
            $path = 'uploads/images/';
            $image = Str::random(4).'-'.Str::random(4);
            $imageName = $image.'.'.$request->file->getClientOriginalExtension();

            $request->file->move($path, $imageName);

            $datos['img_post'] = $path.$imageName;
            Post::where('id', $request->reg)->update($datos);
            $msg = [
                'msg' => 'Imagen actualizada correctamente',
                'tipo' => 'success',
                'icon' => 'bi bi-check-circle',
            ];
        } else {
            $msg = ['type' => 'danger',
                'icon' => 'bi bi-x-circle',
                'msg' => 'Seleccione al menos una imagen.', ];
        }

        return response()->json($msg);
    }

    public function expUsrFile(Request $request){
        $data['file'] = $request->file;
        if ($data['file'] == "xls" || $data['file'] == "pdf") {
            $regs = getSession('regUsersExp');
            if (count($regs) > 0) {
                if ($data['file'] == "pdf") {
                    $data['title'] = "Exportar usuarios a PDF";
                    $pdf = new Mpdf(array('', 'LETTER', 1, '', 5, 5, 5, 5, 2, 2, 'P'));
                    $pdf->setFooter('{PAGENO}');
                    $html = view('postsExport', $data);

                    //$stylesheet = file_get_contents(asset('public/assets/bootstrap/css/pdf.bootstrap.css'));
                    //$pdf->WriteHTML($stylesheet, 1);
                    $pdf->WriteHTML($html, 2);
                    $file = base64_encode($pdf->Output('RPT-USERS-' . fecha(today(), 'd-m-Y') . '.pdf', 'S'));
                    $msg = array(
                        "tipo" => 'success',
                        "icon"     => 'bi bi-check-circle',
                        "file"     => $file,
                        "name"     => 'RPT-USERS-' . fecha(today(), 'd-m-Y'),
                        "msg"     => 'Reporte generado correctamente. Iniciando descarga.'
                    );
                }

                if ($data['file'] == "xls") {
                    $setData         = "";
                    $fecaRegPti     = "";
                    $columnHeader     = "ID" . "\t" . "Título" . "\t" . "Slug" . "\t" . 'Fecha' . "\n";
                    $setData         .= $columnHeader;
                    $content = "";
                    $id = 1;
                    foreach ($regs as $reg) {
                        $content .= $id . "\t" . $reg->nom_post . "\t" . $reg->slug_post . "\t" . $reg->fc_post . "\n";
                        $id++;
                    }
                    $setData .= $content;
                    $file = base64_encode($setData);
                    $msg = array(
                        "tipo" => 'success',
                        "icon"     => 'bi bi-check-circle',
                        "file"     => $file,
                        "name"     => 'RPT-USERS-' . fecha(today(), 'd-m-Y'),
                        "msg"     => 'Registros exportados correctamente. Iniciando descarga.'
                    );
                }
            } else {
                $msg = array(
                    "tipo" => 'danger',
                    "icon" => 'bi bi-x-circle',
                    "msg" => 'No hay registros para exportar.'
                );
            }
        } else {
            $msg = array(
                "tipo" => 'danger',
                "icon" => 'bi bi-x-circle',
                "msg" => 'Seleccione un tipo de archivo válido.'
            );
        }
        return response()->json($msg);

        return response()->json($msg);
    }
}
