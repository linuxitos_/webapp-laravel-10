<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id('id');
            $table->timestamp('fc_post')->useCurrent();
            $table->tinyInteger('act_post')->default(1)->comment('0 deleted, 1 active & private, 2 active & public, 3 active & pending');
            $table->string('nom_post', 300)->nullable()->comment('name post');
            $table->string('slug_post', 512)->default('na-na')->comment('slug post base on name unique');
            $table->longText('desc_post')->nullable()->comment('short description post');
            $table->longText('cnt_post')->nullable()->comment('all contentent post');
            $table->string('dir_post', 100)->nullable()->comment('dir files addedd images or files');
            $table->string('img_post', 120)->nullable()->scomment('email post');
            $table->foreignId('user_id');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
};
