$(document).on("click", ".export-file", function () {
	var file = $(this).data("file");
	$.ajax({
		type: 		"POST",
		dataType: 	"JSON",
		method: 	"POST",
		url: 		base_url + "/expUsrFile",
		data: {
			file: file,
			search: search,
			filter: filter,
			limite: limite,
			order: 	order,
			order_by: 	order_by,
		},
		beforeSend: function(objeto){
			$(".btn-export-file").attr("disabled", true);
			$(".btn-export-file").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Generando');
		},
		success: function(datos){
			$(".btn-export-file").html('<i class="bi bi-download"></i> <span id="spn-export" class="d-none d-md-inline-block">Exportar</span>');
			$(".btn-export-file").attr("disabled", false);
			if (datos.tipo=='success') {
				var sampleArr = base64ToArrayBuffer(datos.file);
				saveByteArray(datos.name, sampleArr, file);
			}
			notify_msg(datos.icon, " ", datos.msg, "#", datos.tipo);
		},
		error: function(e) {
			$(".btn-export-file").html('<i class="bi bi-download"></i> <span id="spn-export" class="d-none d-md-inline-block">Exportar</span>');
			$(".btn-export-file").attr("disabled", false);
			notify_msg("bi bi-x-circle", " ", e.statusText, "#", "danger");
		}
	});
});
