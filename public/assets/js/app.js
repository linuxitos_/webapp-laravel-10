var limite 		= 10;
var filter 		= '1,2';
var order 		= "desc";
var order_by	= "id";
var reg = 0;
var search = "";

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$('.dropdown-limit').find('a').click(function(e) {
	$("#spn-show-list").text($(this).data("total"));
	limite = $(this).data("total");
	load(1);
	e.preventDefault();
});


$('.dropdown-edo').find('a').click(function(e) {
	filter 		= $(this).data("edo");
	$("#i-icon-act").removeAttr("class").attr("class", $(this).data("icon"));
	$("#spn-desc-act").text($(this).data("desc"));
	load(1);
	e.preventDefault();
});


$("#form-search").submit(function( event ) {
	load(1);
	event.preventDefault();
});


function load(page) {
	search 		= $('#txt-search').val();
	$.ajax({
		type: 		'POST',
		url: 		base_url+'/load',
		method: 	'POST',
		dataType: 	'JSON',
		data: {
			page: 	page,
			search: search,
			filter: filter,
			limite: limite,
			order: 	order,
			order_by: order_by,
		},
		beforeSend: function(objeto) {
			$('#btn-search').html('<span class="spinner-border spin-x align-middle" role="status" aria-hidden="true"></span> Buscando...');
		},
		success: function(res) {
			$('#div-cnt-load').html(res.data);
			$('#h5-cnt-total').html(res.total==1?'1 resultado': res.total + ' resultados');
			$('#btn-search').html('<i class="bi bi-search"></i> <span id="spn-desc-buscar" class="d-none d-md-inline-block">Buscar</span>');
		},
		error: function(data) {
			$("#btn-search").html('<i class="bi bi-search"></i> <span id="spn-desc-buscar" class="d-none d-md-inline-block">Buscar</span>');
			$("#div-cnt-load").html('<div class="text-center alert alert-danger" role="alert"><i class="bi bi-exclamation-circle"></i> Error interno, intenta más tarde.</div>');
		}
	});
}


$(document).on("click", ".table th.th-link", function () {
	if (order=="asc") {
		order 	= "desc";
	}else{
		order 	= "asc";
	}
	order_by = $(this).attr("data-field");
	load(1);
});


$(document).on("click", ".btn-show-passwd", function () {
	var inputPass2 = document.getElementById($(this).data('passwd'));
	if ($(this).hasClass("bi-eye")) {
		inputPass2.setAttribute('type', 'password');
		$(this).removeAttr('class').attr('class', 'bi bi-eye-slash form-icon-passwd btn-show-passwd');
		inputPass2.className 	= 'form-control';
	}else{
		inputPass2.setAttribute('type', 'text');
		$(this).removeAttr('class').attr('class', 'bi bi-eye form-icon-passwd btn-show-passwd');
		inputPass2.className 	= 'form-control';
	}
});


$(document).on("click", ".mdl-del-reg", function () {
	$("#txt-id-reg-del").val($(this).data('idreg'));
	$("#txt-nom-reg").text('"'+$(this).data('nomreg')+'"');
});


$("#form-del-reg").submit(function( e ) {
	var data = $(this).serialize();
	var url = base_url+"/delete";
	$('#btn-del-reg').attr("disabled", true);
	$.ajax({
		url:url,
		method:'POST',
		dataType: 'JSON',
		data:data,
		beforeSend: function(objeto) {
			$("#btn-del-regs").html('<span class="spinner-border spin-x align-middle" role="status"><span class="sr-only">Loading...</span></span> Eliminando...');
		},
		success:function(response){
			if(response.type=="success"){
				$("#form-del-reg")[0].reset();
				load(1);
				$("#btn-close-mdl-del-reg").trigger("click");
				notify_msg(response.icon, " ", response.msg, "#", response.type);
			}else{
				$("#div-cnt-del-reg").html('<div class="alert alert-'+response.type+'" role="alert"><i class="'+response.icon+'"></i> '+response.msg+'</div>');
			}
			$('#btn-del-reg').attr("disabled", false);
			$("#btn-del-reg").html('<i class="bi bi-trash3"></i> Eliminar');
		},
		error:function(error){
			$('#btn-del-reg').attr("disabled", false);
			$("#btn-del-reg").html('<i class="bi bi-trash3"></i> Eliminar');
			$("#div-cnt-del-reg").html('<div class="alert alert-danger" role="alert"><i class="bi bi-exclamation-circle"></i> Error interno, intenta más tarde.</div>');
		}
	});
	e.preventDefault();
});


$(document).on("submit", ".form-add-reg", function (e) {
    const editorData = editor.getData();
    $(".cnt-post").val(editorData);
    if ($(".cnt-post").val() != '') {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            method: "POST",
            url: base_url + "/addPost",
            data: new FormData(this),
            contentType: false,
            //cache: false,
            processData: false,
            beforeSend: function (objeto) {
                $("#btn-add-post").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Validando...');
                $('#btn-add-post').attr("disabled", true);
            },
            success: function (datos) {
                $("#btn-add-post").html('<i class="bi bi-check-circle"></i> Continuar');
                $('#btn-add-post').attr("disabled", false);
                if (datos.tipo == "success") {
                    $("#btn-add-post").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Redireccionando');
                    $('#btn-add-post').attr("disabled", true);
                    setTimeout(function () {
                        $(window).attr('location', datos.url);
                    }, 2000);
                }
                if (datos.errors) {
                    jQuery.each(datos.errors, function (key, value) {
                        notify_msg('bi bi-x-circle', " ", value, "#", 'danger');
                    });
                } else {
                    notify_msg(datos.icon, " ", datos.msg, "#", datos.tipo);
                }
            },
            error: function (data) {
                $("#btn-add-post").html('<i class="bi bi-check-circle"></i> Continuar');
                $("#btn-add-post").attr("disabled", false);
                notify_msg("bi bi-x-circle", " ", data.statusText, "#", "danger");
            }
        });
    } else {
        notify_msg("bi bi-x-circle", " ", "El contenido del artículo es necesario.", "#", "danger");
    }
    e.preventDefault();
});


$(document).on("submit", ".form-up-pos", function (e) {
    const editorData = editor.getData();
    $(".cnt-post").val(editorData);
    if ($(".cnt-post").val() != '') {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            method: "POST",
            url: base_url + "/upPost",
            data: new FormData(this),
            contentType: false,
            //cache: false,
            processData: false,
            beforeSend: function (objeto) {
                $("#btn-up-post").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Validando...');
                $('#btn-up-post').attr("disabled", true);
            },
            success: function (datos) {
                $("#btn-up-post").html('<i class="bi bi-check-circle"></i> Actualizar');
                $('#btn-up-post').attr("disabled", false);
                if (datos.tipo == "success") {
                    $("#btn-up-post").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Actualizando');
                    $('#btn-up-post').attr("disabled", true);
                    setTimeout(function () {
                        $(window).attr('location', datos.url);
                    }, 2000);
                }
                if (datos.errors) {
                    jQuery.each(datos.errors, function (key, value) {
                        notify_msg('bi bi-x-circle', " ", value, "#", 'danger');
                    });
                } else {
                    notify_msg(datos.icon, " ", datos.msg, "#", datos.tipo);
                }
            },
            error: function (data) {
                $("#btn-up-post").html('<i class="bi bi-check-circle"></i> Actualizar');
                $("#btn-up-post").attr("disabled", false);
                notify_msg("bi bi-x-circle", " ", data.statusText, "#", "danger");
            }
        });
    } else {
        notify_msg("bi bi-x-circle", " ", "El contenido del artículo es necesario.", "#", "danger");
    }
    e.preventDefault();
});


function loadImagePost() {
    $.ajax({
        url: base_url + "/loadImagePost",
        method: "POST",
        dataType: "JSON",
        type: "POST",
        data: {
            reg: reg,
        },
        beforeSend: function (objeto) {
            $("#div-cnt-reg").html('<div class="alert alert-dark text-center" role="alert">' +
                '<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Cargando</div>');
        },
        success: function (data) {
            $("#div-cnt-reg").html(data.results);
        },
        error: function (response) {
            $("#div-cnt-reg").html('<div class="alert alert-danger text-center" role="alert"><i class="bi bi-x-circle align-middle"></i> ' +
                'Error interno, intenta más tarde.</div>');
        }
    });
};

$(document).on("change", ".formAddImgAny", function (e) {
    var fl = document.getElementById('fileimages');
    var ln = fl.files.length;
    var formData 	= new FormData();

    if (ln <= 0) {
        notify_msg("bi bi-x-circle", " ", "Seleccione al menos una imagen.", "#", "danger");
        return;
    } else {
        for (var i = 0; i<ln; i++) {
            formData.append('file', $('#fileimages')[0].files[i]);
            formData.append("reg", reg);
            $.ajax({
                url: base_url + "/upImgPost",
                data: formData,
                type: 'POST',
                contentType: false,
                //cache: false,
                processData: false,
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('#progUpAnyImg').text(percentComplete + '%');
                            $('#progUpAnyImg').css('width', percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                beforeSend: function (objeto) {
                    $('#progUpAnyImg').removeAttr("class").attr("class", "bg-success text-center");
                    $('#progUpAnyImg').css('width', '0');
                    $('#btnUploadImgAny').attr("disabled", true);
                    $('#btnUploadImgAny').html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span>');
                },
                success: function (data) {
                    $('#progUpAnyImg').css('width', 100 + '%');
                    $('#progUpAnyImg').text('0%');
                    if (data.tipo == 'success') {
                        setTimeout(function () {
                            $('#progUpAnyImg').removeAttr("class").attr("class", "bg-default text-center");
                        }, 2000);
                        loadImagePost();
                    } else {
                        $('#progUpAnyImg').removeAttr("class").attr("class", "bg-danger text-center");
                        setTimeout(function () {
                            $('#progUpAnyImg').removeAttr("class").attr("class", "bg-default text-center");
                        }, 2000);
                    }
                    $('#btnUploadImgAny').attr("disabled", false);
                    $('#btnUploadImgAny').html('<i class="bi bi-cloud-upload"></i> Subir');
                    notify_msg(data.icon, " ", data.msg, "#", data.tipo);
                },
                error: function (data) {
                    $('#btnUploadImgAny').attr("disabled", false);
                    $('#progUpAnyImg').css('width', 100 + '%');
                    $('#progUpAnyImg').text('0%');
                    $('#progUpAnyImg').removeAttr("class").attr("class", "bg-danger text-center");
                    $('#btnUploadImgAny').html('<i class="bi bi-cloud-upload"></i> Subir');
                    setTimeout(function () {
                        $('#progUpAnyImg').removeAttr("class").attr("class", "bg-default text-center");
                        $('#progUpAnyImg').text('0%');
                    }, 2000);
                    notify_msg("bi bi-x-circle", " ", "Error interno, intenta más tarde.", "#", "danger");
                }
            });
        }
    }
    e.preventDefault();
});

$(document).on("click", ".btn-del-img-profile", function () {
	var id = $(this).data('id');
	$.ajax({
		type: "POST",
		url: base_url + "/delImgProfile",
		dataType: "JSON",
		data: {
			id: id,
		},
		beforeSend: function (objeto) {
			$(".btn-del-img-profile").html('<div class="spinner-border spin-x" role="status" aria-hidden="true"></div>');
			$('#btn-del-img-profile').prop('disabled', true);
		},
		success: function (datos) {
			$(".btn-del-img-profile").html('<i class="bi bi-x fs-2"></i>');
			$('#btn-del-img-profile').prop('disabled', false);
			notify_msg(datos.icon, " ", datos.msg, "#", datos.tipo);
			if (datos.tipo == "success") {
				$("#btn-del-img-profile").removeClass('').addClass('d-none');
				$("#div-cnt-img-profile").html('<img class="image-input-wrapper w-125px h-125px" src="'+datos.img+'">');
			}
		},
		error: function (datos) {
			$(".btn-del-img-profile").html('<i class="bi bi-x fs-2"></i>');
			$('#btn-del-img-profile').prop('disabled', false);
			notify_msg("bi bi-x-circle", " ", "Error en la petición, intenta más tarde", "#", "danger");
		}
	});
});


$(document).on("click", ".export-file", function () {
	var file = $(this).data("file");
	$.ajax({
		type: 		"POST",
		dataType: 	"JSON",
		method: 	"POST",
		url: 		base_url + "/expUsrFile",
		data: {
			file: file,
			search: search,
			filter: filter,
			limite: limite,
			order: 	order,
			order_by: 	order_by,
		},
		beforeSend: function(objeto){
			$(".btn-export-file").attr("disabled", true);
			$(".btn-export-file").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Generando');
		},
		success: function(datos){
			$(".btn-export-file").html('<i class="bi bi-download"></i> <span id="spn-export" class="d-none d-md-inline-block">Exportar</span>');
			$(".btn-export-file").attr("disabled", false);
			if (datos.tipo=='success') {
				var sampleArr = base64ToArrayBuffer(datos.file);
				saveByteArray(datos.name, sampleArr, file);
			}
			notify_msg(datos.icon, " ", datos.msg, "#", datos.tipo);
		},
		error: function(e) {
			$(".btn-export-file").html('<i class="bi bi-download"></i> <span id="spn-export" class="d-none d-md-inline-block">Exportar</span>');
			$(".btn-export-file").attr("disabled", false);
			notify_msg("bi bi-x-circle", " ", e.statusText, "#", "danger");
		}
	});
});

function base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}

function saveByteArray(reportName, byte, file) {
    var blob = new Blob([byte]);
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    var fileName = reportName + "." + file;
    link.download = fileName;
    link.click();
}
