@extends('layouts.app')

@section('content')
@include('modals')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{url('/')}}"><i class="bi bi-house"></i> Inicio</a>
					</li>
                    <li class="breadcrumb-item">
						<a href="{{route('posts')}}"><i class="bi bi-list-task"></i> Registros</a>
					</li>
					<li class="breadcrumb-item active" aria-current="page">
                        <i class="bi bi-pencil-square"></i> Cuenta
					</li>
				</ol>
			</nav>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">
                        <i class="bi bi-pencil-square"></i> Información de la cuenta
                    </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="row">
                                <div id="div-cnt-reg" class="col-md-12">

                                </div>
                                <div class="col-md-12 mb-2">
                                    <div class="input-group custom-file-button">
                                        <input class="form-control upProfileImg" type="file" id="fileimages" name="files[]"  required="required" multiple="multiple" accept="image/*">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="progress">
                                        <div id="progUpAnyImg" class="progress-bar bg-default" role="progressbar" style="width: 100%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">0%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <form  class="form-up-account mt-3" method="post" enctype="multipart/form-data" accept-charset="UTF-8">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-6 mb-3">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input id="email" type="email" class="form-control float-form @error('email') is-invalid @enderror" placeholder=" " required="required" autocomplete="off" value="{{auth()->user()->email}}" readonly disabled/>
                                                <label for="email">Email*</label>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-md-6 mb-3">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <i class="bi bi-card-text form-control-icon"></i>
                                                <input id="name" type="text" class="form-control float-form @error('name') is-invalid @enderror" placeholder=" " required="required" autocomplete="off" name="name" autofocus value="{{auth()->user()->name}}"/>
                                                <label for="name">Nombre*</label>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-6 mt-2 mb-2">
                                        <button type="submit" class="btn btn-success w-100" id="btn-up-post">
                                            <i class="bi bi-check-circle"></i> Actualizar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{asset('assets/js/account.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        loadImageUser();
    });
</script>
@endsection
