@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <i class="bi bi-person-plus"></i> {{ __('Registro') }}
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-6">
                                <div class="has-float-label">
                                    <i class="bi bi-person-circle form-control-icon"></i>
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder=" ">
                                    <label for="name">Nombre</label>
                                    @error('name')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-6">
                                <div class="has-float-label">
                                    <i class="bi bi-at form-control-icon"></i>
                                    <input id="email" type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder=" " required autofocus autocomplete="off" value="{{ old('email') }}"/>
                                    <label for="email">Email</label>
                                    @error('email')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-6">
                                <div class="has-float-label">
                                    <i class="bi bi-shield-lock form-control-icon"></i>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder=" ">
                                    <label for="password">Contraseña</label>
                                    @error('password')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                    @enderror
                                    <i id="icon-eye1" class="bi bi-eye-slash form-icon-passwd btn-show-passwd" data-passwd="password"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-6">
                                <div class="has-float-label">
                                    <i class="bi bi-shield-lock form-control-icon"></i>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder=" " value="">
                                    <label for="password-confirm">Repetir contraseña</label>
                                    <i id="icon-eye2" class="bi bi-eye-slash form-icon-passwd btn-show-passwd" data-passwd="password-confirm"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-0 justify-content-center">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary float-end">
                                    <i class="bi bi-check-circle"></i> {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
