@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <i class="bi bi-box-arrow-in-right"></i>     {{ __('Iniciar sesión') }}
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row mb-3 mt-3 justify-content-center">
                            <div class="col-md-6">
                                <div class="form-group input-group">
                                    <span class="has-float-label">
                                        <i class="bi bi-person-circle form-control-icon"></i>
                                        <input id="email" type="email" class="form-control float-form @error('email') is-invalid @enderror" placeholder=" " required="required" autocomplete="off" name="email" value="{{ old('email') }}" autofocus/>
                                        <label for="email">Usuario</label>
                                        @error('email')
                                            <div class="invalid-feedback" role="alert">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3 mt-3 justify-content-center">
                            <div class="col-md-6">
                                <div class="form-group input-group">
                                    <span class="has-float-label">
                                        <i class="bi bi-shield-lock form-control-icon"></i>
                                        <input id="password" type="password" class="form-control float-form @error('password') is-invalid @enderror" placeholder=" " required="required" autocomplete="off" name="password" value="{{ old('password') }}" autofocus autocomplete="off"/>
                                        <label for="password">Contraseña</label>
                                        <i class="bi bi-eye-slash form-icon-passwd btn-show-passwd" data-passwd="password"></i>
                                    </span>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-2">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        {{ __('Recuérdame') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-3 col-6">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('¿Olvidaste la contraseña?') }}
                                    </a>
                                @endif
                            </div>
                            <div class="col-md-3 col-6">
                                <button type="submit" class="btn btn-primary float-end">
                                    <i class="bi bi-box-arrow-in-right"></i> {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
