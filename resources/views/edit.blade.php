@extends('layouts.app')

@section('content')
@include('modals')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{url('/')}}"><i class="bi bi-house"></i> Inicio</a>
					</li>
                    <li class="breadcrumb-item">
						<a href="{{route('posts')}}"><i class="bi bi-list-task"></i> Registros</a>
					</li>
					<li class="breadcrumb-item active" aria-current="page">
                        <i class="bi bi-pencil-square"></i> Editando
					</li>
				</ol>
			</nav>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">
                        <i class="bi bi-pencil-square"></i> Editando nuevo post
                    </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="row">
                                <div id="div-cnt-reg" class="col-md-12">

                                </div>
                                <div class="col-md-12 mb-2">
                                    <div class="input-group custom-file-button">
                                        <input class="form-control formAddImgAny" type="file" id="fileimages" name="files[]"  required="required" multiple="multiple" accept="image/*">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="progress">
                                        <div id="progUpAnyImg" class="progress-bar bg-default" role="progressbar" style="width: 100%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">0%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <form  class="form-up-pos mt-3" method="post" enctype="multipart/form-data" accept-charset="UTF-8">
                                @csrf
                                <input type="hidden" name="id" value="{{$post->id}}" readonly>
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <i class="bi bi-card-text form-control-icon"></i>
                                                <input id="nom_post" type="text" class="form-control float-form @error('nom_post') is-invalid @enderror" placeholder=" " required="required" autocomplete="off" name="nom_post" autofocus value="{{$post->nom_post}}"/>
                                                <label for="nom_post">Título*</label>
                                                @error('nom_post')
                                                    <div class="invalid-feedback" role="alert">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-6 mb-3">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input id="fc_post" type="date" class="form-control float-form date @error('fc_post') is-invalid @enderror" placeholder=" " required="required" autocomplete="off" name="fc_post" value="{{fecha($post->fc_post, 'Y-m-d')}}" autofocus/>
                                                <label for="fc_post">Fecha*</label>
                                                @error('fc_post')
                                                    <div class="invalid-feedback" role="alert">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 mb-3">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <select class="form-select" aria-label="Default select example" id="act_post" name="act_post" required>
                                                    <option value="1" {{$post->act_post==1?'selected':''}}>Privado</option>
                                                    <option value="2" {{$post->act_post==2?'selected':''}}>Público</option>
                                                  </select>
                                                <label for="act_post">Estatus*</label>
                                                @error('fc_post')
                                                    <div class="invalid-feedback" role="alert">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-md-12 mb-3">
                                        <label for="cnt_post">Contenido*</label>
                                        <textarea name="cnt_post" rows="2" placeholder=" " class="cnt-post form-control" autocomplete="off" id="cnt_post" style="display: none;">{{$post->cnt_post}}</textarea>
                                    </div>
                                </div>
                                <div class="row justify-content-end mb-6 mt-3">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-6 mt-2 mb-2">
                                        <button type="submit" class="btn btn-success w-100" id="btn-up-post">
                                            <i class="bi bi-check-circle"></i> Actualizar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('script')
    <script type="text/javascript">
		$(document).ready(function() {
            reg = {{$post->id}};
            loadImagePost();
            let editor;
            ClassicEditor.create(document.querySelector('.cnt-post'), {
                ckfinder: {
                    uploadUrl: base_url+'/upload',
                },
                height: '300px',
                toolbar: {
                    items: [
                        'heading',
                        '|',
                        'bold',
                        'italic',
                        'link',
                        'bulletedList',
                        'numberedList',
                        '|',
                        'outdent',
                        'indent',
                        '|',
                        'imageUpload',
                        'blockQuote',
                        'insertTable',
                        'undo',
                        'redo',
                        'alignment',
                        'fontSize'
                    ]
                },
                language: 'es',
                image: {
                    styles: [
                        'alignLeft', 'alignCenter', 'alignRight'
                    ],
                    resizeOptions: [
                        {
                            name: 'resizeImage:original',
                            label: 'Original',
                            value: null
                        },
                        {
                            name: 'resizeImage:50',
                            label: '50%',
                            value: '50'
                        },
                        {
                            name: 'resizeImage:75',
                            label: '75%',
                            value: '75'
                        }
                    ],
                    toolbar: [
                        'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                        '|',
                        'resizeImage',
                        '|',
                        'imageTextAlternative'
                    ],
                },
                table: {
                    contentToolbar: [
                        'tableColumn',
                        'tableRow',
                        'mergeTableCells'
                    ]
                },
            })
            .then( newEditor => {
                window.editor = newEditor;
                editor = newEditor;
            })
            .catch( error => {
                console.error( 'Oops, something went wrong!' );
                console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
                console.warn( 'Build id: q6l505nuvif2-xw3ce1wx5aqw' );
                console.error( error );
            });
		});
    </script>
@endsection
