@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h3>Contenido</h3>
            <hr>
        </div>
    </div>

    <div class="row">
        @if(!empty($data) && $data->count())
            @foreach($data as $post)
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 mb-3 d-flex">
                    <article class="border rounded">
                        <figure class="rounded overflow-hidden mb-3 bsb-overlay-hover post">
                            <a href="/{{$post->slug_post}}">
                                <img class="img-fluid bsb-scale bsb-hover-scale-up" loading="lazy" src="{{asset('assets/imgs/default.png')}}" alt="Careers">
                            </a>
                        </figure>
                        <div class="entry-header mb-2 pe-2 px-2">
                            <h2 class="entry-title h4 mb-0">
                                <a class=" text-decoration-none" href="{{$post->slug_post}}">
                                    {{$post->nom_post}}
                                </a>
                            </h2>
                        </div>
                        <div class="entry-footer">
                            <ul class="entry-meta list-unstyled d-flex align-items-center mb-0 p-2 justify-content-center">
                                <li>
                                    <a class="fs-7 link-secondary text-decoration-none d-flex align-items-center" href="#!">
                                        <i class="bi bi-calendar-week"></i>
                                        <span class="ms-2 fs-7">
                                            {{ucfirst(nicetime($post->fc_post))}}
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <span class="px-3">•</span>
                                </li>
                                <li>
                                    <a class="link-secondary text-decoration-none d-flex align-items-center" href="#!">
                                        <i class="bi bi-chat-dots"></i>
                                        <span class="ms-2 fs-7">0</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </article>
                </div>
            @endforeach
        @else
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <i class="fa fa-exclamaiton-circle"></i> Sin resultados.
                </div>
            </div>
        @endif
        <div class="col-md-12 d-flex justify-content-center">
            {!! $data->links() !!}
        </div>
    </div>
</div>
@endsection
