<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-bs-theme="dark">
<head>
    @include('layouts.partials._head')
</head>
<body>
    <div id="app">
        @include('layouts.partials._navbar')
        <main class="py-4">
            @yield('content')
        </main>

        @include('layouts.partials._foot')
        @yield('script')
    </div>
</body>
</html>
