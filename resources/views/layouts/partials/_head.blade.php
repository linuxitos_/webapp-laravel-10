<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="{{ asset('assets/imgs/favicon.png')}}" type="image/png">
<meta name="csrf-token" content="{{ csrf_token()}}">
<title>{{!empty($title)?$title.' | '.env('APP_NAME'): env('APP_NAME')}}</title>

<link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/bootstrap/css/animate.css')}}">
<link rel="stylesheet" href="{{asset('assets/bootstrap/css/animation.css')}}">
<link rel="stylesheet" href="{{asset('assets/bootstrap-icons/bootstrap-icons.min.css')}}">

<link rel="stylesheet" href="{{asset('assets/css/app.css')}}">

<style>
    .fstElement {
        font-size: 0.6em;
    }
    .fstToggleBtn {
        min-width: 2em;
    }
    .submitBtn {
        display: none;
    }
    .fstMultipleMode {
        display: block;
    }
    .fstMultipleMode .fstControls {
        width: 100%;
    }
    .multipleSelect{
        overflow-x: auto !important;
        max-height: 20px !important;
        z-index: 3898;
    }
    .google-maps iframe {
        width: 100% !important;
    }
</style>

<script>
    var base_url = "{{route('/')}}";
    var subsec 	= "start";
    var sec 	= "ini";
    var hostUrl = "assets/";
</script>
