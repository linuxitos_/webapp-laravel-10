<nav class="navbar navbar-expand-md navbar-light bg-dark shadow-sm">
	<div class="container">
		<button class="navbar-toggler" class="btn btn-primary text-white" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasWithBothOptions" aria-controls="offcanvasWithBothOptions">
			<i class="bi bi-layout-text-sidebar text-white"></i>
		</button>
		<a class="navbar-brand" href="{{ route('/') }}">
			<img class="img-fluid" src="{{ asset('assets/imgs/logo.svg')}}" id="logo_custom" alt="logo" width="100">
		</a>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav me-auto">

			</ul>
			<ul class="navbar-nav ms-auto">
				@guest
					@if (!Route::is('login'))
						<li class="nav-item">
							<a class="btn btn-outline-primary me-2" href="{{ route('login') }}">{{ __('Iniciar sesión') }}</a>
						</li>
					@endif

					@if (!Route::is('register'))
						<li class="nav-item">
							<a class="btn btn-outline-warning" href="{{ route('register') }}">{{ __('Registro') }}</a>
						</li>
					@endif
				@else
					<li class="nav-item dropdown">
						<a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <img src="{{(!is_null(auth()->user()->avatar) && auth()->user()->avatar!=''? asset(auth()->user()->avatar) : asset('assets/imgs/user.svg'))}}" alt="mdo" width="32" height="32" class="rounded-circle"> {{ Auth::user()->name }}
						</a>

						<div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">

							<a class="dropdown-item {{Route::is('posts')?'active':''}}" href="{{ route('posts') }}">
								Posts <i class="bi bi-grid float-end text-primary"></i>
							</a>

							<a class="dropdown-item {{Route::is('account')?'active':''}}" href="{{ route('account') }}">
								Cuenta <i class="bi bi-person-square float-end text-success"></i>
							</a>
							<hr class="dropdown-divider">
							<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								Salir <i class="bi bi-power float-end text-danger"></i>
							</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
								@csrf
							</form>
						</div>
					</li>
				@endguest
			</ul>
		</div>
		<div class="dropdown text-end">
			<button class="text-decoration-none text-white dropdown-toggle btn btn-link" id="bd-theme" type="button" aria-expanded="false" data-bs-toggle="dropdown" data-bs-display="static" aria-label="Toggle theme (dark)">
				<i id="i-icon-them" class="bi bi-sun-fill text-white"></i>
			</button>
			<ul class="dropdown-menu text-small dropdown-menu-end" aria-labelledby="bd-theme-text">
				<li>
					<button type="button" class="dropdown-item active" data-bs-theme-value="light" aria-pressed="true">
						Ligth <i class="bi bi-sun-fill float-end"></i>
					</button>
				</li>
				<li>
					<button type="button" class="dropdown-item" data-bs-theme-value="dark" aria-pressed="false">
						Dark <i class="bi bi-moon-stars-fill float-end"></i>
					</button>
				</li>
			</ul>
		</div>
	</div>
</nav>


<div class="offcanvas offcanvas-start" data-bs-scroll="true" tabindex="-1" id="offcanvasWithBothOptions" aria-labelledby="offcanvasWithBothOptionsLabel">
	<div class="offcanvas-header bg-dark text-white">
		<a href="{{ route('/')}}" class="navbar-brand text-white" title="Inicio">
			<img class="img-fluid" src="{{ asset('assets/imgs/logo.svg')}}" id="logo_custom" width="100" alt="logo">
		</a>
		<button type="button" class="btn-close text-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
	</div>
	<div class="offcanvas-body">
        @guest
            <div class="row">
                <div class="col-md-12 text-center">
                    @if (!Route::is('login'))
                        <a class="btn btn-primary me-2" href="{{ route('login') }}">
                            <i class="bi bi-box-arrow-in-right"></i> {{ __('Iniciar sesión') }}
                        </a>
                    @endif

                    @if (!Route::is('register'))
                        <a class="btn btn-outline-success" href="{{ route('register') }}">
                            <i class="bi bi-plus-circle"></i> {{ __('Registro') }}
                        </a>
                    @endif
                </div>
            </div>
        @else
            <div class="">
                <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-body-emphasis text-decoration-none">
                    <span class="fs-4">Panel</span>
                </a>
                <hr>
                <ul class="nav nav-pills flex-column mb-auto">
                    <li class="nav-item">
                        <a href="{{ route('/') }}" class="nav-link {{Route::is('/')?'active':''}}" aria-current="page">
                            <i class="bi bi-house-fill"></i> Inicio
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('posts') }}" class="nav-link link-body-emphasis {{Route::is('posts')?'active':''}}">
                            <i class="bi bi-grid"></i> Posts
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('account') }}" class="nav-link link-body-emphasis {{Route::is('account')?'active':''}}">
                            <i class="bi bi-person-square"></i> Cuenta
                        </a>
                    </li>
                </ul>
                <hr>
                <div class="dropdown">
                    <a href="#" class="d-flex align-items-center link-body-emphasis text-decoration-none dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="{{(!is_null(auth()->user()->avatar) && auth()->user()->avatar!=''? asset(auth()->user()->avatar) : asset('assets/imgs/user.svg'))}}" alt="" width="32" height="32" class="rounded-circle me-2">
                        <strong>{{ Auth::user()->name }}</strong>
                    </a>
                    <ul class="dropdown-menu text-small shadow" style="">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-logout').submit();">
                            Salir <i class="bi bi-power float-end text-danger"></i>
                        </a>
                        <form id="logout-form-logout" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </ul>
                </div>
            </div>
        @endguest
	</div>
</div>
