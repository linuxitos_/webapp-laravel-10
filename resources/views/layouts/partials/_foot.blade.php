<!-- /jquery -->
<script src="{{asset('assets/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/jquery/jquery-3.7.1.min.js')}}"></script>
<script src="{{asset('assets/jquery/theme.js')}}"></script>

<script src="{{asset('assets/ckeditor5/build/ckeditor.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/js/app.js')}}"></script>
<!-- /jquery -->

<script src="{{asset('assets/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-notify/message-notify.js')}}"></script>


<script type="text/javascript">
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
        }

        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            let stateObj = { id: "100" };
            window.history.replaceState(stateObj,"Page 3", e.target.hash);
        });

        if (sec!='ini') {
            $('.kt_app_sidebar_menu').animate({
                scrollTop: $('#'+subsec).offset().top-500
            }, 'slow');
        }
    });
    $('.dropdown-not-close').on('click', function(event){
        event.stopPropagation();
    });
</script>
