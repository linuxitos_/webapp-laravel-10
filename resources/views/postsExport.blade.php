<div id="container">
    <div id="body" style="margin-top: 0px;">
        <table cellspacing="0" cellpadding="1" border="0" style="border:0px none black;">
            <tr>
                <td width="100%" style="text-align:right;">
                    <span style="font-size:15px;">Reporte en PDF de Información de Empleados</span><br>
                    <span style="font-size:13px;">Generado el día <?=fecha(today(), 'd-m-Y')?></span><br>
                </td>
            </tr>
        </table>
        @php
        $regs = getSession('regUsersExp');
        @endphp
        @if (count($regs)>0)
            <table  class="mt-3">
                <thead>
                    <tr style="border-bottom:1px solid;">
                        <th width="5%">#</th>
                        <th width="20%">Nombre</th>
                        <th width="25%">Slug</th>
                        <th width="25%">Fecha</th>
                    </tr>
                </thead>
                <tbody id="fileList">
                    @foreach ($regs as $reg)
                        <tr style="border-bottom:1px solid gray;">
                            <td>{{$loop->iteration}}</td>
                            <td>{{$reg->nom_post}}</td>
                            <td>{{$reg->slug_post}}</td>
                            <td>{{ fecha($reg->fc_post) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
            <p class="text-center mt-4">Sin resultados</p>
        @endif
    </div>
</div>
