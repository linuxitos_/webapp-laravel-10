@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-ekt">
                    <h4>
                        Transferencia
                    </h4>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1">
                                    Folio transferencia:
                                </span>
                                <input type="text" class="form-control" placeholder="Folio transferencia" aria-label="Username" aria-describedby="basic-addon1" value="8838298">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-success">Buscar</button>
                        </div>
                        <div class="col-md-3">
                            <span class="input-group-text justify-center" id="basic-addon1">
                                100 - EKT CDMX
                            </span>
                        </div>

                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-6">
                            <h5 class="d-inline">
                                Detalle de la Transferencia:
                            </h5>
                        </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="inputGroup-sizing-default">Fecha solicitud</span>
                                <input type="date" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="{{date('Y-m-d')}}">
                              </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-striped">
                                    <thead class="table-danger bg-ekt">
                                        <tr>
                                            <th scope="col">SKU</th>
                                            <th scope="col">Cant. Solicitada</th>
                                            <th scope="col">Cant. Enviada</th>
                                            <th scope="col">Series Enviada</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">2901920</th>
                                            <td>3</td>
                                            <td>3</td>
                                            <td>SSDLKDSD,LSDKSDLK,LKDS9334LKDS</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2920192</th>
                                            <td>3</td>
                                            <td>2</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">29019292</th>
                                            <td>3</td>
                                            <td>2</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">21920192</th>
                                            <td>3</td>
                                            <td>2</td>
                                            <td>AKKSAL,AKS989394,SKDJSKD83</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                            <h5 class="d-inline">
                                Transporte
                            </h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="inputGroup-sizing-default">Chofer:</span>
                                <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="BAZ">
                              </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="inputGroup-sizing-default">Placa:</span>
                                <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="19X0AS">
                              </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="inputGroup-sizing-default">Línea:</span>
                                <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" value="EntSD">
                              </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary btn-lg float-end">Guardar</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
