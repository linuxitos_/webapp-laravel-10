@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row g-5">
            <div class="col-md-8">
                <article class="blog-post">
                    <h2 class="display-5 link-body-emphasis mb-1">{{$data['post']->nom_post}}</h2>
                    <p class="blog-post-meta">{{nicetime($data['post']->fc_post)}} - <a href="/{{$data['post']->slug_post}}">{{$data['post']->author->name}}</a></p>
                    <hr>
                    {!!$data['post']->cnt_post!!}
                </article>
            </div>

            <div class="col-md-4">
                <div class="position-sticky" style="top: 2rem;">
                <div class="p-4 mb-3 bg-body-tertiary rounded">
                    <h4 class="fst-italic">Acerca de:</h4>
                    <p class="mb-0">
                        Un blog de pruebas para proyectos simples y proyectos de universidad.
                    </p>
                </div>
                <div>
                    <h4 class="fst-italic">Entradas recientes</h4>
                    <ul class="list-unstyled">
                        @foreach ($data['latest'] as $item)
                        <li>
                            <a class="d-flex flex-column flex-lg-row gap-3 align-items-start align-items-lg-center py-3 link-body-emphasis text-decoration-none border-top" href="/{{$item->slug_post}}">
                                <img src="{{ !is_null($data['post']->img_post) && $data['post']->img_post!=''? asset($item->img_post) : asset('assets/imgs/default.png') }}" alt="" class="bd-placeholder-img" width="100%" height="96">
                                <div class="col-lg-8">
                                    <h6 class="mb-0">{{$item->nom_post}}</h6>
                                    <small class="text-body-secondary">{{nicetime($data['post']->fc_post)}}</small>
                                </div>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
