@extends('layouts.app')

@section('content')
@include('modals')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{url('/')}}"><i class="bi bi-house"></i> Inicio</a>
					</li>
					<li class="breadcrumb-item active" aria-current="page">
                        <i class="bi bi-list-task"></i> Registros
					</li>
				</ol>
			</nav>
        </div>
    </div>

    <div class="row">
		<div class="col-lg-8 col-md-7 col-sm-8 col-8">
			<form id="form-search" name="form-search" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                <div class="input-group">
                    <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <span class="d-none d-md-inline-block bi bi-list-task"></span> <span id="spn-show-list">10</span>
                    </button>
                    <ul id="select-list" class="dropdown-menu dropdown-limit">
                        <li>
                            <a class="dropdown-item" href="#" data-total="10">
                                <i class="bi bi-list-task"></i> 10
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#" data-total="20">
                                <i class="bi bi-list-task"></i> 20
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#" data-total="30">
                                <i class="bi bi-list-task"></i> 30
                            </a>
                        </li>
                    </ul>
                    <input type="text" id="txt-search" name="txt-search" class="form-control txt-search-nv" aria-label="Búsqueda" placeholder="Búsqueda">
                    <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <i id="i-icon-act" class="bi bi-check-circle"></i> <span id="spn-desc-act" class="d-none d-md-inline-block">Todos</span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-edo">
                        <li>
                            <a class="dropdown-item" href="#" data-desc="Privados" data-icon="bi bi-lock" data-edo="1">
                                <i class="bi bi-lock"></i> Privados
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#" data-desc="Publicados" data-icon="bi bi-globe" data-edo="2">
                                <i class="bi bi-globe"></i> Publicados
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#" data-desc="Todos" data-icon="bi bi-list-task" data-edo="1,2">
                                <i class="bi bi-list-task"></i> Todos
                            </a>
                        </li>
                    </ul>
                    <button id="btn-search" class="btn btn-outline-secondary" type="submit">
                        <i class="bi bi-search"></i> <span id="spn-desc-buscar" class="d-none d-md-inline-block">Buscar</span>
                    </button>
                </div>
			</form>
		</div>
		<div class="col-lg-4 col-md-5 col-sm-4 col-4">
            <div class="input-group justify-content-end">
                <a class="btn btn-success" href="{{route('add')}}">
                    <i class="bi bi-plus-circle"></i> <span id="spn-desc-add" class="d-none d-md-inline-block">Agregar</span>
                </a>
                <button id="btn-export-file" type="button" class="btn btn-secondary dropdown-toggle btn-export-file" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="bi bi-share"></i> <span id="spn-export" class="d-none d-md-inline-block">Exportar</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-end desplegable-menu">
                    <li>
                        <a class="dropdown-item export-file" href="#" data-desc="Excel" data-icon="bi bi-file-earmark-spreadsheet" data-file="xls">
                            <i class="bi bi-file-earmark-spreadsheet text-success"></i> Excel
                        </a>
                    </li>
                    <li>
                        <a class="dropdown-item export-file" href="#" data-desc="PDF" data-icon="bi bi-file-earmark-pdf" data-file="pdf">
                            <i class="bi bi-file-earmark-pdf-fill text-danger"></i> PDF
                        </a>
                    </li>
                </ul>
            </div>
		</div>
	</div>

    <div class="row">
		<div class="col-md-12 mb-2 mt-3">
			<span id="h5-cnt-total" class="float-end"></span>
		</div>
	</div>
	<div class="row">
		<div id="div-cnt-load" class="col-md-12"></div>
	</div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            load(1);
        });
    </script>
@endsection
